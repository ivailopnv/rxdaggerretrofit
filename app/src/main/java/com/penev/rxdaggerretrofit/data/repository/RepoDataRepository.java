package com.penev.rxdaggerretrofit.data.repository;


import com.penev.rxdaggerretrofit.data.mapper.RepoDataMapper;
import com.penev.rxdaggerretrofit.data.repository.datasource.RepoDataStoreFactory;
import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.domain.repository.RepoRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class RepoDataRepository implements RepoRepository {

	private RepoDataStoreFactory mRepoDataStoreFactory;

	@Inject
	RepoDataRepository(RepoDataStoreFactory repoDataStoreFactory) {
		mRepoDataStoreFactory = repoDataStoreFactory;
	}

	@Override
	public Observable<List<RepoEntity>> repos() {
		return mRepoDataStoreFactory.create().repoEntityList().map(RepoDataMapper::transform);
	}

	@Override
	public Observable<RepoEntity> repo(int repoId) {
		return mRepoDataStoreFactory.create().repoEntityDetails(repoId).map(RepoDataMapper::transform);
	}
}
