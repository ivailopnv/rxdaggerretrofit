package com.penev.rxdaggerretrofit.data.mapper;


import com.penev.rxdaggerretrofit.data.model.RepoData;
import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

public class RepoDataMapper {

	public static RepoEntity transform(@Nonnull RepoData repoData) {
		RepoEntity repoEntity = new RepoEntity();
		repoEntity.setId(repoData.getId());
		repoEntity.setName(repoData.getName());
		return repoEntity;
	}

	public static List<RepoEntity>transform(@Nonnull List<RepoData> repoDataList) {
		List<RepoEntity> entityList = new ArrayList<>();

		for (RepoData repoData : repoDataList) {
			RepoEntity repoEntity = transform(repoData);
			entityList.add(repoEntity);
		}

		return entityList;
	}
}
