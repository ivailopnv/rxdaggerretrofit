package com.penev.rxdaggerretrofit.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RepoData {

	@SerializedName("id")
	@Expose
	private Integer id;

	@SerializedName("name")
	@Expose
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

}
