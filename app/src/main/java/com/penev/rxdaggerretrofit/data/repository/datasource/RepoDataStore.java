package com.penev.rxdaggerretrofit.data.repository.datasource;


import com.penev.rxdaggerretrofit.data.model.RepoData;

import java.util.List;

import io.reactivex.Observable;

public interface RepoDataStore {

	Observable<List<RepoData>> repoEntityList();

	Observable<RepoData> repoEntityDetails(int id);
}
