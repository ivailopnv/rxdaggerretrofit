package com.penev.rxdaggerretrofit.data.repository.datasource;


import javax.inject.Inject;

public class RepoDataStoreFactory {

	private CloudRepoDataStore mCloudRepoDataStore;

	@Inject
	RepoDataStoreFactory(CloudRepoDataStore cloudRepoDataStore){
		mCloudRepoDataStore = cloudRepoDataStore;
	}

	public RepoDataStore create() {
		//todo cash or network logic should be here
		return mCloudRepoDataStore;
	}
}
