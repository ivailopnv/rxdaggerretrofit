package com.penev.rxdaggerretrofit.data.repository.datasource;


import com.penev.rxdaggerretrofit.data.model.RepoData;
import com.penev.rxdaggerretrofit.data.net.RestApi;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CloudRepoDataStore implements RepoDataStore {

	public static final String ACCESS_TOKEN = "60f08769d7ba0487f881dbecfe836ebc70a64740";

	private RestApi mRestApi;

	@Inject
	CloudRepoDataStore(RestApi restApi) {
		mRestApi = restApi;
	}

	@Override
	public Observable<List<RepoData>> repoEntityList() {
		HashMap<String, String> options = new HashMap<>();
		options.put("access_token", ACCESS_TOKEN);
		return mRestApi.getIvoPnvRepos(options);
	}

	@Override
	public Observable<RepoData> repoEntityDetails(int id) {
		HashMap<String, String> options = new HashMap<>();
		options.put("access_token", ACCESS_TOKEN);
		return mRestApi.getRepoById(String.valueOf(id), options);
	}
}
