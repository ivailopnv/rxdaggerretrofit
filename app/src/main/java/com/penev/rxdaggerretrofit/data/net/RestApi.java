package com.penev.rxdaggerretrofit.data.net;


import com.penev.rxdaggerretrofit.data.model.RepoData;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RestApi {

	@GET("users/ivopnv/repos")
	Observable<List<RepoData>> getIvoPnvRepos(@QueryMap Map<String, String> options);

	@GET("repositories/{id}")
	Observable<RepoData> getRepoById(@Path("id")String repoId, @QueryMap Map<String, String> options);

}
