package com.penev.rxdaggerretrofit.presentation.view.holder;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.penev.rxdaggerretrofit.R;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.utils.Navigator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoViewHolder extends RecyclerView.ViewHolder {

	@BindView(R.id.repo_name_text_view) TextView mRepoNameTextView;
	@BindView(R.id.repo_id_text_view) TextView mRepoIdTextView;

	private RepoModel mRepoModel;

	private Context mContext;

	@Inject
	public RepoViewHolder(View itemView, Context context) {
		super(itemView);
		ButterKnife.bind(this, itemView);
		mContext = context;
	}

	public void bind(final RepoModel repoModel) {
		mRepoModel = repoModel;
		mRepoNameTextView.setText(repoModel.getName());
		mRepoIdTextView.setText(String.valueOf(repoModel.getId()));
		itemView.setOnClickListener(view -> Navigator.startActivity(mContext, mRepoModel.getId()));
	}
}
