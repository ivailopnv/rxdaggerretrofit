package com.penev.rxdaggerretrofit.presentation.di.modules;

import com.penev.rxdaggerretrofit.data.net.RestApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GitHubModule {

	private static final String BASE_URL = "https://api.github.com/";

	@Provides
	@Singleton
	Retrofit provideRetrofit() {
		return new Retrofit.Builder()
				.addConverterFactory(GsonConverterFactory.create())
				.client(new OkHttpClient.Builder().build())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.baseUrl(BASE_URL)
				.build();
	}

	@Provides
	@Singleton
	RestApi provideRetroFitService (Retrofit retrofit) {
		return retrofit.create(RestApi.class);
	}
}
