package com.penev.rxdaggerretrofit.presentation.view;


import com.penev.rxdaggerretrofit.presentation.model.RepoModel;

import java.util.Collection;

public interface RepoListView {

	void showLoading();

	void hideLoading();

	void renderRepos(Collection<RepoModel> reposCollection);
}
