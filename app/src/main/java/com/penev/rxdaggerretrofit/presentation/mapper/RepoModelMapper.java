package com.penev.rxdaggerretrofit.presentation.mapper;


import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

public class RepoModelMapper {

	public static RepoModel transform(@Nonnull RepoEntity repoEntity) {
		RepoModel repoModel = new RepoModel();
		repoModel.setId(repoEntity.getId());
		repoModel.setName(repoEntity.getName());
		return repoModel;
	}

	public static List<RepoModel> transform(@Nonnull List<RepoEntity> repoEntities) {
		List<RepoModel> repoModels = new ArrayList<>();

		for (RepoEntity repoEntity : repoEntities) {
			RepoModel repoModel = transform(repoEntity);
			repoModels.add(repoModel);
		}

		return repoModels;
	}
}
