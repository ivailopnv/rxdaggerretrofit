package com.penev.rxdaggerretrofit.presentation.view.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.penev.rxdaggerretrofit.R;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.view.holder.RepoViewHolder;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

public class RepoAdapter extends RecyclerView.Adapter<RepoViewHolder> {

	private List<RepoModel> mRepoModelList;

	private LayoutInflater mLayoutInflater;

	private Context mContext;

	@Inject
	RepoAdapter(Context context) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = mLayoutInflater.inflate(R.layout.layout_repo_item, parent, false);
		return new RepoViewHolder(view, mContext);
	}

	@Override
	public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
		holder.bind(mRepoModelList.get(position));
	}

	@Override
	public int getItemCount() {
		return (mRepoModelList != null) ? mRepoModelList.size() : 0;
	}

	public void bindRepos(Collection<RepoModel> repoModels) {
		mRepoModelList = (List<RepoModel>)repoModels;
		notifyDataSetChanged();
	}
}
