package com.penev.rxdaggerretrofit.presentation.di.modules;

import com.penev.rxdaggerretrofit.presentation.view.activity.RepoDetailsActivity;
import com.penev.rxdaggerretrofit.presentation.view.activity.RepoListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyApplicationModule {

	@ContributesAndroidInjector
	abstract RepoListActivity contributeActivityListInjector();

	@ContributesAndroidInjector
	abstract RepoDetailsActivity contributeActivityDetailsInjector();

}
