package com.penev.rxdaggerretrofit.presentation.presenter;

import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.domain.interactors.GetRepoDetails;
import com.penev.rxdaggerretrofit.presentation.mapper.RepoModelMapper;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.view.RepoDetailsView;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class RepoDetailsPresenter implements Presenter {

	private RepoDetailsView mRepoDetailsView;

	@Inject GetRepoDetails mGetRepoDetailsUseCase;

	@Inject RepoDetailsPresenter() {

	}

	public void setView(RepoDetailsView repoDetailsView) {
		mRepoDetailsView = repoDetailsView;
	}

	@Override
	public void resume() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void destroy() {
		mRepoDetailsView = null;
	}

	@Override
	public void showLoading() {
		mRepoDetailsView.showLoading();
	}

	@Override
	public void hideLoading() {
		mRepoDetailsView.hideLoading();
	}

	private void renderRepo(RepoModel repo) {
		mRepoDetailsView.renderRepo(repo);
	}

	public void loadRepo(int id) {
		showLoading();
		mGetRepoDetailsUseCase.execute(new RepoDetailsObserver(), id);
	}

	private final class RepoDetailsObserver extends DisposableObserver<RepoEntity> {

		@Override
		public void onComplete() {}

		@Override
		public void onNext(RepoEntity repoEntity) {
			hideLoading();
			renderRepo(RepoModelMapper.transform(repoEntity));
		}

		@Override
		public void onError(Throwable e) {}
	}
}
