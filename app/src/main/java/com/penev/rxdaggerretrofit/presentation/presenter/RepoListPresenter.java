package com.penev.rxdaggerretrofit.presentation.presenter;


import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.domain.interactors.GetReposList;
import com.penev.rxdaggerretrofit.presentation.mapper.RepoModelMapper;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.view.RepoListView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class RepoListPresenter implements Presenter {

	private RepoListView mRepoListView;

	@Inject GetReposList mGetReposUseCase;

	@Inject
	RepoListPresenter() {

	}

	public void setView(RepoListView repoListView) {
		mRepoListView = repoListView;
	}

	@Override
	public void resume() {}


	@Override
	public void pause() {

	}

	@Override
	public void destroy() {
		mRepoListView = null;
	}

	@Override
	public void showLoading() {
		mRepoListView.showLoading();
	}

	@Override
	public void hideLoading() {
		mRepoListView.hideLoading();
	}

	private void renderRepos(Collection<RepoModel> repoModels) {
		mRepoListView.renderRepos(repoModels);
	}

	public void loadRepos() {
		showLoading();
		mGetReposUseCase.execute(new UserListObserver(), null);
	}

	private final class UserListObserver extends DisposableObserver<List<RepoEntity>> {

		@Override
		public void onComplete() {}

		@Override
		public void onNext(List<RepoEntity> repoModels) {
			hideLoading();
			renderRepos(RepoModelMapper.transform(repoModels));
		}

		@Override
		public void onError(Throwable e) {}
	}
}
