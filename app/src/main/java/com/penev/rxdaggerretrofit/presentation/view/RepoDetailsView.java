package com.penev.rxdaggerretrofit.presentation.view;

import com.penev.rxdaggerretrofit.presentation.model.RepoModel;

public interface RepoDetailsView {

	void showLoading();

	void hideLoading();

	void renderRepo(RepoModel repo);
}
