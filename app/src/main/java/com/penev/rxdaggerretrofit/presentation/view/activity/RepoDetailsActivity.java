package com.penev.rxdaggerretrofit.presentation.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.penev.rxdaggerretrofit.R;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.presenter.RepoDetailsPresenter;
import com.penev.rxdaggerretrofit.presentation.view.RepoDetailsView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class RepoDetailsActivity extends AppCompatActivity implements RepoDetailsView {

	public static final String EXTRA_REPO_ID = "com.penev.extra.REPO_ID";

	@BindView(R.id.repo_name) TextView mRepoNameTextView;
	@BindView(R.id.repo_id) TextView mRepoIdTextView;

	@BindView(R.id.progress) ProgressBar mProgressBar;

	@Inject RepoDetailsPresenter mRepoDetailsPresenter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		AndroidInjection.inject(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_repo_details);
		ButterKnife.bind(this);
		int id = getIntent().getIntExtra(EXTRA_REPO_ID, 0);
		mRepoDetailsPresenter.setView(this);
		mRepoDetailsPresenter.loadRepo(id);
	}

	@Override
	public void showLoading() {
		mProgressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void hideLoading() {
		mProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void renderRepo(RepoModel repo) {
		mRepoNameTextView.setText(repo.getName().toUpperCase());
		mRepoIdTextView.setText(String.valueOf(repo.getId()));
	}
}
