package com.penev.rxdaggerretrofit.presentation.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.penev.rxdaggerretrofit.presentation.view.activity.RepoDetailsActivity;

import static com.penev.rxdaggerretrofit.presentation.utils.Const.TAG;

public class Navigator {

	public static void startActivity(Context context, int repoId) {
		Intent i = new Intent(context, RepoDetailsActivity.class);
		i.putExtra(RepoDetailsActivity.EXTRA_REPO_ID, repoId);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		try {
			context.startActivity(i);
		} catch (ActivityNotFoundException e) {
			Log.e(TAG, e.toString());
		}
	}
}
