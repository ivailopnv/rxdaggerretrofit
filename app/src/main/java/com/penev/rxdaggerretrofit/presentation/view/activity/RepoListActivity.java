package com.penev.rxdaggerretrofit.presentation.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.penev.rxdaggerretrofit.R;
import com.penev.rxdaggerretrofit.presentation.model.RepoModel;
import com.penev.rxdaggerretrofit.presentation.presenter.RepoListPresenter;
import com.penev.rxdaggerretrofit.presentation.view.RepoListView;
import com.penev.rxdaggerretrofit.presentation.view.adapter.RepoAdapter;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;


public class RepoListActivity extends AppCompatActivity implements RepoListView {

	@BindView(R.id.repos_recycler_view) RecyclerView mReposRecyclerView;

	@BindView(R.id.progress_bar) ProgressBar mProgressBar;

	@Inject RepoListPresenter mRepoListPresenter;

	@Inject RepoAdapter mRepoAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		AndroidInjection.inject(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_repo_list);
		ButterKnife.bind(this);
		mRepoListPresenter.setView(this);
		mRepoListPresenter.loadRepos();
		mReposRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mReposRecyclerView.setAdapter(mRepoAdapter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mRepoListPresenter.resume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mRepoListPresenter.pause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mRepoListPresenter.destroy();
	}

	@Override
	public void showLoading() {
		mProgressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void hideLoading() {
		mProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void renderRepos(Collection<RepoModel> reposCollection) {
		mRepoAdapter.bindRepos(reposCollection);
	}
}
