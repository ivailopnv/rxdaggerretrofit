package com.penev.rxdaggerretrofit.presentation.di.components;


import com.penev.rxdaggerretrofit.MyApplication;
import com.penev.rxdaggerretrofit.presentation.di.modules.ApplicationModule;
import com.penev.rxdaggerretrofit.presentation.di.modules.GitHubModule;
import com.penev.rxdaggerretrofit.presentation.di.modules.MyApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationModule.class,
		GitHubModule.class, MyApplicationModule.class})
public interface MyApplicationComponent extends AndroidInjector<MyApplication> {

}
