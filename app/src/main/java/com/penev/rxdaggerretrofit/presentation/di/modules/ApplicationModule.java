package com.penev.rxdaggerretrofit.presentation.di.modules;

import android.content.Context;

import com.penev.rxdaggerretrofit.data.repository.RepoDataRepository;
import com.penev.rxdaggerretrofit.domain.repository.RepoRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

	private Context mContext;

	public ApplicationModule(Context context) {
		mContext = context;
	}

	@Provides
	@Singleton
	Context providesApplicationContext() {
		return mContext;
	}

	@Provides
	@Singleton
	RepoRepository providesRepoRepository(RepoDataRepository repoDataRepository) {
		return repoDataRepository;
	}
}
