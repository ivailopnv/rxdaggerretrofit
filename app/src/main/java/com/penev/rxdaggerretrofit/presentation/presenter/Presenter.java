package com.penev.rxdaggerretrofit.presentation.presenter;


public interface Presenter {

	void resume();

	void pause();

	void destroy();

	void showLoading();

	void hideLoading();
}
