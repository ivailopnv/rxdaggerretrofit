package com.penev.rxdaggerretrofit.domain.interactors;


import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class UseCase<T, Params> {

	abstract Observable<T> buildUseCaseObservable(Params params);

	public void execute(Observer<T> observer, Params params) {
		buildUseCaseObservable(params)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(observer);
	}
}
