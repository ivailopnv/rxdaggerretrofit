package com.penev.rxdaggerretrofit.domain.interactors;

import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.domain.repository.RepoRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetRepoDetails extends UseCase<RepoEntity, Integer> {

	private RepoRepository mRepoRepository;

	@Inject
	GetRepoDetails(RepoRepository repoRepository) {
		mRepoRepository = repoRepository;
	}

	@Override
	Observable<RepoEntity> buildUseCaseObservable(Integer id) {
		return mRepoRepository.repo(id);
	}
}
