package com.penev.rxdaggerretrofit.domain.interactors;


import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;
import com.penev.rxdaggerretrofit.domain.repository.RepoRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetReposList extends UseCase<List<RepoEntity>, Void> {

	private RepoRepository mRepoRepository;

	@Inject
	GetReposList(RepoRepository repoRepository) {
		mRepoRepository = repoRepository;
	}

	@Override
	Observable<List<RepoEntity>> buildUseCaseObservable(Void unused) {
		return mRepoRepository.repos();
	}
}
