package com.penev.rxdaggerretrofit.domain.repository;


import com.penev.rxdaggerretrofit.domain.entity.RepoEntity;

import java.util.List;


import io.reactivex.Observable;

public interface RepoRepository {

	Observable<List<RepoEntity>> repos();

	Observable<RepoEntity> repo(int repoId);
}
