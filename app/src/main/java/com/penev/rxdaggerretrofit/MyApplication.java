package com.penev.rxdaggerretrofit;


import android.app.Activity;
import android.app.Application;


import com.penev.rxdaggerretrofit.presentation.di.components.DaggerMyApplicationComponent;
import com.penev.rxdaggerretrofit.presentation.di.modules.ApplicationModule;
import com.penev.rxdaggerretrofit.presentation.di.modules.GitHubModule;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class MyApplication extends Application implements HasActivityInjector {

	@Inject
	DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

	@Override
	public void onCreate() {
		super.onCreate();
		DaggerMyApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.gitHubModule(new GitHubModule())
				.build().inject(this);
	}

	@Override
	public DispatchingAndroidInjector<Activity> activityInjector() {
		return dispatchingAndroidInjector;
	}
}
